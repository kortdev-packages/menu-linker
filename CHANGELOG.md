# Changelog

All notable changes to `kortdev/menu-linker` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## 0.0.9 - 2020-09-14

- added Laravel Telescope (5.0) support
- added Laravel Horizon (5.0) support

