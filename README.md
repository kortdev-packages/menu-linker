# Kort.dev Nova Menu Linker
[![Latest Version on Packagist](https://img.shields.io/packagist/v/kortdev/menu-linker.svg?style=flat-square)](https://packagist.org/packages/kortdev/menu-linker)
[![Total Downloads](https://img.shields.io/packagist/dt/kortdev/menu-linker.svg?style=flat-square)](https://packagist.org/packages/kortdev/menu-linker)

## Combined from
 - [Laravel Nova Telescope Smart Link](https://github.com/mad-web/nova-telescope-link)
 - [Laravel Nova Horizon Smart Link](https://github.com/mad-web/nova-horizon-link)

### Laravel Nova Telescope Smart Link
Ability to add link to the [Laravel Telescope](https://github.com/laravel/telescope)
Assistant in the [Laravel Nova](https://nova.laravel.com/) sidebar. Link automatically disables if current user hasn't access to the _Telescope Debug Assistant_ according
with access policy which is defined in gate `viewTelescope`. For more information, checkout the [documentation](https://github.com/laravel/telescope#dashboard-authorization).

### Laravel Nova Horizon Smart Link
Ability to add link to the [Laravel Horizon](https://horizon.laravel.com/)
Dashboard in the [Laravel Nova](https://nova.laravel.com/) sidebar. Link automatically disables if current user hasn't access to the _Horizon Dashboard_ according
with access policy which is defined in `Horizon::auth` method. For more information, checkout the [documentation](https://laravel.com/docs/master/horizon#dashboard-authentication).

## Installation
You can install the nova theme into a Laravel app that uses [Nova](https://nova.laravel.com) via composer:
```bash
composer require kortdev/menu-linker
```

Next up, register of the links in the `tools` method of the `NovaServiceProvider`:

```php
// app/Providers/NovaServiceProvider.php

// ...

public function tools()
{
    return [
        // ...
        new \Kortdev\MenuLinker\TelescopeLink,
        new \Kortdev\MenuLinker\HorizonLink,
    ];
}
```

## Customization & More information

Please see Laravel Nova Telescope Smart Link](https://github.com/mad-web/nova-telescope-link) or [Laravel Nova Horizon Smart Link](https://github.com/mad-web/nova-horizon-link) for more details

## Security

If you discover any security related issues, please email [lars@kort.dev](mailto:lars@kort.dev) instead of using the issue tracker.

## Credits
- [LTKort](https://kort.dev/)
- [Mad Web](https://github.com/mad-web)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
